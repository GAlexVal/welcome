from django.apps import AppConfig


class WelcomeConfig(AppConfig):
    name = 'DjangoVue2.apps.welcome'
